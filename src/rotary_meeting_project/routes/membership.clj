(ns rotary-meeting-project.routes.membership
    (:require 
     [compojure.core :refer :all]
     [rotary-meeting-project.models.membership :as membership]))

(defroutes membership-routes
  (GET "/membership/:id" [id] (membership/get-membership-res id))
  (POST "/membership/:id" [id clubs_id start end] (membership/update-membership-res {:id id :clubs_id clubs_id :start start :end end}))
  (POST "/membership" [clubs_id users_id start end] (membership/save-membership-res {:users_id users_id :clubs_id clubs_id :start start :end end})))


