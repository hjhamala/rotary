(ns rotary-meeting-project.routes.user
  (:require 
    [compojure.core :refer :all]
    [rotary-meeting-project.models.user :as user]
    [rotary-meeting-project.models.util :as u]
    [ring.middleware.multipart-params :as multipart]))
            
(defroutes user-routes
  (GET "/user" [] (user/getusers-res))
  (GET "/user/:id" [id]  (user/getuser-res (u/get-int id)))
  (GET "/user/:id/club-history" [id]  (user/get-club-history-res (u/get-int id)))
  (DELETE "/user/:id" [id] (user/deleteuser-res (u/get-int id)))
  (POST "/user" [firstname surname id] (user/saveuser-res {:firstname firstname :surname surname :id (u/get-int id)}))
  (multipart/wrap-multipart-params (POST "/user/:id/image" {params :params} (user/add-image params))))
