(ns rotary-meeting-project.routes.club
  (:require 
     [compojure.core :refer :all]
     [rotary-meeting-project.models.util :as u]
     [rotary-meeting-project.models.club :as club]))

(defroutes club-routes
  (GET "/club" [] (club/get-clubs-res))
  (GET "/club/:clubs_id/memberships" [clubs_id date] (club/get-club-members-res (u/get-int clubs_id) date))
  (GET "/club/:clubs_id" [clubs_id] (club/get-club-res (u/get-int clubs_id)))
  (POST "/club" [id established name club_rotary_id] (club/save-club-res {:id (u/get-int id) :established established :name name :club_rotary_id (u/get-int club_rotary_id)})))
