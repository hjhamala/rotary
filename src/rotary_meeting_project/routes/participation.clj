(ns rotary-meeting-project.routes.participation
   (:require 
     [compojure.core :refer :all]
     [rotary-meeting-project.models.participation :as participation]))

(defroutes participation-routes
  ;; Routes for participating meetings
  (POST "/participation" [meetings_id memberships_id] (participation/participate-res meetings_id memberships_id))
  (DELETE "/participation/:meetings_id/:memberships_id" [memberships_id meetings_id] (participation/unparticipate-res meetings_id memberships_id)))
