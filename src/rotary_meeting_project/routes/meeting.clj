(ns rotary-meeting-project.routes.meeting
   (:require 
     [compojure.core :refer :all]
     [rotary-meeting-project.models.meeting :as meeting]
     [buddy.auth.backends.token :refer [token-backend]]))

(defroutes meeting-routes
  (GET "/meeting/:id/participation-statistics" [id] (meeting/get-meeting-statistics-res id))
  (GET "/meeting/:id/participation-list" [id] (meeting/get-participation-list-res id))
  (GET "/meeting" [id] (meeting/get-meetings-res))
  (GET "/meeting/:id" [id] (meeting/get-meeting-res id))
  (POST "/meeting" [id meeting_date topic presenter clubs_id] (meeting/save-meeting-res {:id id :meeting_date meeting_date :topic topic :presenter presenter :clubs_id clubs_id})))
  