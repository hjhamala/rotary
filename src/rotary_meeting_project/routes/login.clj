(ns rotary-meeting-project.routes.login
  (:require 
     [compojure.core :refer :all]
     [rotary-meeting-project.models.authentication :as authentication]
     [buddy.auth :refer [authenticated? throw-unauthorized]]
     [buddy.auth.backends.token :refer [token-backend]]
     [rotary-meeting-project.models.util :as u]))

(def auth-backend
  (token-backend {:authfn authentication/authenticate}))

(defroutes login-routes
  (POST "/login" [username password](authentication/login-res username password))
  (GET "/login" {:keys [headers params body] :as request} (if (authenticated? request)
                                                            (u/ok {:logged true :identity (:identity request)})
                                                            (u/unauthorized {:error "Not logged"}))) 
  (DELETE "/login/:token" [token] (authentication/logoff-res token)))

