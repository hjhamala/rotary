(ns rotary-meeting-project.handler
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [compojure.handler :as handler]
            [rotary-meeting-project.models.util :as u]
            [rotary-meeting-project.models.authentication :as auth]
            [rotary-meeting-project.routes.user :as user]
            [rotary-meeting-project.routes.club :as club]
            [rotary-meeting-project.routes.meeting :as meeting]
            [rotary-meeting-project.routes.participation :as participation]
            [rotary-meeting-project.routes.membership :as membership]
            [rotary-meeting-project.routes.login :as login]
            [rotary-meeting-project.models.sqlite :as db]
            [ring.adapter.jetty :as ring]
            [ring.util.response :as response]
            [ring.middleware.json :as json]
            [ring.middleware.file :as file]
            [ring.middleware.resource :as resource]
            [ring.middleware.not-modified :as not-modified]
            [ring.middleware.content-type :as content-type]
            [buddy.auth.middleware :refer [wrap-authentication wrap-authorization]]
            [buddy.auth.accessrules :refer [wrap-access-rules]]
     (:gen-class)))


(defroutes other-routes
  (GET "/" [] (response/content-type (response/resource-response "index.html" {:root "public"}) "text/html"))
  (route/not-found "Not Found"))

(def app
  (-> 
  (handler/api 
      (-> 
        (routes 
          club/club-routes
          login/login-routes
          meeting/meeting-routes
          membership/membership-routes
          participation/participation-routes
          user/user-routes
          meeting/meeting-routes
          other-routes) 
        (wrap-access-rules {:rules auth/access-rules})
        (wrap-authentication login/auth-backend)
        (file/wrap-file "resources")
        (resource/wrap-resource "public")
        (content-type/wrap-content-type)
        (json/wrap-json-body)
        (json/wrap-json-response)
        (json/wrap-json-params)))))

(defn start 
  [port]
  (db/schema-create)
  (ring/run-jetty #'app {:port port
                         :join? false}))

(defn -main 
  []
  (let [port (Integer. (or (System/getenv "PORT") "8080"))]
    (start port)))
