(ns rotary-meeting-project.models.meeting  
  (:require [rotary-meeting-project.models.sqlite :as sqlite]
            [rotary-meeting-project.models.util :as u]
            [clj-time.core :as time]
            [korma.core :refer :all]
            [rotary-meeting-project.models.kormaentities :refer :all]
            [bouncer.core :as b]
            [bouncer.validators :as v]))
  

(defn get-meeting
  "Return meeting by id"
  [id]
  (first (select meetings (where {:id id}))))

(defn get-meeting-res
  "Return meeting wrapped in response"
  [id]
  (if-let [meeting  (get-meeting id)]
    (u/ok meeting)
    (u/not-found "Meeting not found")))

(defn participating-count
  "Return count of members participating"
  [meetings_id]
  ;; There seems to be a problem with korma and aggregates so lets use raw korma
  (:count (first (exec-raw ["SELECT count(*) as count FROM participations WHERE meetings_id = ?" [meetings_id]] :results))))
 
(defn not-participating-count
  [meetings_id]
  (let [{:keys [meeting_date clubs_id]} (get-meeting meetings_id)]
  ;; There seems to be a problem with korma and aggregates so lets use raw korma
  (:count 
    (first 
      (exec-raw 
        ["SELECT count(*) count 
          FROM memberships 
          INNER JOIN users 
            ON users.id = memberships.users_id 
            AND memberships.start <= ? AND (memberships.end >= ? OR memberships.end IS null)
          LEFT JOIN participations ON memberships.id = participations.memberships_id and participations.meetings_id = ?
               WHERE clubs_id = ? and participations.meetings_id is null;" [meeting_date meeting_date meetings_id clubs_id]] :results)))))

(defn get-meeting-statistics-res 
  [meetings_id]
  (let [participating-count (participating-count meetings_id) 
        not-participating-count (not-participating-count meetings_id)]
    (u/ok {:participating_count participating-count 
           :not_participating_count not-participating-count
           :total (+ participating-count not-participating-count)})))  
                                            
(defn get-participation-list-res
  [id]
  (let [{:keys [meeting_date clubs_id]} (get-meeting id)]
    (->>     
    (select users 
            (fields :id :firstname :surname :image_url [:memberships.id "memberships.id"] [:participations.meetings_id "participations.meetings_id"]) 
            (join :inner memberships) 
            (join :left participations 
                  (and (= :participations.memberships_id :memberships.id) (= :participations.meetings_id id)))
                    ;; Limit memberships to current members
                    (where {:memberships.start [<= meeting_date] 
                            :memberships.clubs_id clubs_id})
                    (where 
                      (or {:memberships.end [>= meeting_date]} 
                          {:memberships.end nil})))
    (map (fn[x](dissoc (if (nil? (:participations.meetings_id x)) x
             (-> x (assoc-in [:participation] true)))
                     :meetings_id)))
    (u/ok))))

(defn save-meeting-res 
  [meeting]
  (let [validation-result (b/validate meeting
              :meeting_date v/required
              :presenter v/required
              :topic v/required
              :clubs_id v/required)] 
  (if (nil? (first validation-result))
    (let [mtg (select-keys meeting [:id :meeting_date :topic :presenter :clubs_id])] ;; Let strip possible extra fields 
      (if (= nil (:id mtg)) 
        (u/save-ok (get-meeting 
                     (sqlite/last-id 
                       (insert meetings (values mtg)))))        
        (cond 
          (= 0 (update meetings  
                       (set-fields mtg) 
                       (where {:id (mtg :id)}))) 
          (u/update-failed {:error "Meeting not updated"})
          :else (u/ok (get-meeting (:id mtg))))))
  (u/bad-request (last validation-result)))))

(defn get-meetings
  []
  (select meetings 
          (with clubs)))

(defn get-meetings-res
  []
  (u/ok (get-meetings)))