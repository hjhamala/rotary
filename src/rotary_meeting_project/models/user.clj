(ns rotary-meeting-project.models.user 
  (:require [rotary-meeting-project.models.util :as u]
            [rotary-meeting-project.models.kormaentities :refer :all]
            [rotary-meeting-project.models.sqlite :as sqlite]
            [clojure.java.io :as io]
            [bouncer.core :as b]
            [clj-time.core :as t]
            [bouncer.validators :as v]
            [korma.core :refer :all]
            [validateur.validation :refer :all]))

(defn getuser-public-info
  "Only public info"
  [id]
  (first (select users (fields :firstname :surname :username) 
                 (where {:id id}))))

(defn getuser
  [id]
  (first (select users 
                 (where {:id id}) 
                 (with clubs 
                   (order :memberships.start :desc) 
                   (limit 1)))))

(defn getuser-res
  [id]
  (let [validation-rules {:id [v/required v/number]}
        validation-map {:id id}]
    (if (b/valid? validation-map validation-rules)
      (if-let[result (getuser id)]
        (u/ok result)
        (u/not-found {:error "User not found"}))
      (u/bad-request {:error (b/validate validation-map validation-rules)}))))

(defn getusers
  []
  (select users 
          (with clubs 
            (order :memberships.start :desc) 
            (limit 1))))

(defn getusers-res
  []
  (u/ok (getusers)))

(defn saveuser
  [user]
  (let [user (assoc-in 
               (select-keys user [:id :firstname :surname :rotaryid :image_url]) [:image_url] "/appimg/image-not-found.jpg")]
    (getuser (sqlite/last-id 
           (insert users (values user))))))

(defn updateuser
  [user]
  (let [user (select-keys user [:id :firstname :surname :rotaryid :image_url])]
    (update users  (set-fields user) (where {:id (user :id)}))))

(defn validation-error-res
  [validation-map status-code]
  {:status status-code :body (:bouncer.core/errors (last validation-map))})

(defn saveuser-res
  [user]
  (let [validation-result (b/validate user
              :firstname v/required
              :surname v/required)] 
  (if (nil? (first validation-result))
    (if (= nil (:id user)) 
      (u/save-ok (saveuser user))        
      (cond 
        (= 0 (updateuser user)) (u/update-failed {:error "Update failed"})
        :else (getuser-res (:id user))))
    (u/bad-request {:error (last validation-result)}))))

(defn deleteuser
  [id]
  (delete users (where {:id id})))

(defn deleteuser-res
  [id]
  (let [validation-rules {:id [v/required v/number]}
        validation-map {:id id}]
    (if (b/valid? validation-map validation-rules)
      (u/ok (deleteuser id))
      (u/bad-request {:error (b/validate validation-map validation-rules)}))))


;; TODO needs proper validation and no hard coded image extension 
(defn add-image
  [file]
  (let [filename (str 1 "-" (t/now) ".jpg")] 
  (io/copy (:tempfile (file "file")) (io/file "resources" "img/" filename))
  (updateuser (assoc-in (getuser (:id file)) [:image_url] (str "/img/" filename)))
  (u/ok (getuser (:id file)))))

(defn get-club-history
  [users_id]
  (select memberships 
          (with clubs (fields :name)) 
          (where {:users_id users_id}) 
          (order :start :ASC)))

(defn get-club-history-res
  [id]
  (let [validation-rules {:id [v/required v/number]}
        validation-map {:id id}]
    (if (b/valid? validation-map validation-rules )
      (u/ok (get-club-history id))
      (u/bad-request (b/validate validation-map validation-rules)))))