(ns rotary-meeting-project.models.sqlite
  (require
    [clojure.string :as str]
    [clojure.java.io :as io]
    [clojure.java.jdbc :as sql]
    [korma.db :refer :all]
    [korma.core :refer :all]
    [buddy.hashers :as hs]))

(def last-id 
  "Shortcut to get last id from sqlite"
  (keyword "last_insert_rowid()")) 

(def sqlite-db {:subprotocol "sqlite"
                :subname "resources/db/rotary.db"})

(def sqlite-db-test {:subprotocol "sqlite"
                     :subname "resources/db/rotary-test.db"})

(defdb korma-db sqlite-db)

(defdb korma-db-test sqlite-db-test)
  
(def create-users (   
 sql/create-table-ddl :users
   [:id "integer primary key"]
   [:username "varchar(64)"]
   [:password "char(162)"]
   [:firstname "varchar(255)"]
   [:surname "varchar(255)"]
   [:rotaryid "integer"]
   [:image_url "varchar(255)"])
)

(def create-authentications (   
 sql/create-table-ddl :authentications
   [:id "integer primary key"]
   [:token "char(32) not null"] 
   [:valid "date not null"]
   [:users_id "integer not null" "references clubs(id)"]))

(def create-meetings (   
 sql/create-table-ddl :meetings
   [:id "integer primary key"]
   [:meeting_date "date"]
   [:topic "varchar(255)"]
   [:presenter "integer"]
   [:clubs_id "integer not null" "references clubs(id)"]))

(def create-clubs (   
 sql/create-table-ddl :clubs
   [:id "integer primary key"]
   [:club_rotary_id "integer"]
   [:name "varchar(255)"]
   [:established "date"])
)

(def create-memberships (   
 sql/create-table-ddl :memberships
 ;; Easier to have extra id for primary key
 [:id "integer primary key"]
 [:users_id "integer not null" "references users(id)"]
 [:clubs_id "integer not null" "references clubs(id)"]
 [:start "date not null"]
 [:end "date"]))

(def create-participations (   
 sql/create-table-ddl :participations
   [:meetings_id "integer not null" "references meetings(id)"]
   [:memberships_id "integer not null" "references memberships(id)"]
   ["PRIMARY KEY(meetings_id, memberships_id)"])
)

(defn drop-database
  [db]
  (io/delete-file (db :subname) true ))

(defn populate-users
  [db] (sql/insert! db  :users
         {:id 3 :firstname "Heikki" :surname "Hämäläinen" :username "hjhamala" :password (hs/encrypt "heikki") :image_url "/appimg/image-not-found.jpg"}
         {:id 1 :firstname "Pentti" :surname "Holopainen" :image_url "/appimg/image-not-found.jpg"}
         {:id 8 :firstname "Gun" :surname "Hanson" :image_url "/appimg/image-not-found.jpg"}
         {:id 26 :firstname "Not" :surname "Member" :image_url "/appimg/image-not-found.jpg"}))

(defn populate-clubs
  [db] (sql/insert! db  :clubs
         {:id 4 :name "Kluuvin Rotaryklubi"}
         {:id 2 :name "Kampin Rotaryklubi"}))

(defn populate-meetings
  [db] (sql/insert! db  :meetings
         {:id 8 :meeting_date "2015-03-03" :topic "Talvisodan henki" :clubs_id 4}
         {:id 4 :meeting_date "2013-03-10" :topic "Jatkosodan henki" :clubs_id 2}))


(defn populate-memberships
  [db] 
  (sql/insert! db  :memberships            
    {:id 3 :start "2013-01-01" :end "2014-12-31" :users_id 1 :clubs_id 2}
    {:id 100 :start "2014-01-01" :users_id 1 :clubs_id 4}
    
    {:id 1 :start "2013-01-01" :users_id 8 :clubs_id 4}
    {:id 5 :start "2012-01-01" :end "2012-12-31" :users_id 8 :clubs_id 2}
    
    {:id 11 :start "2013-01-01" :users_id 3 :clubs_id 4}
    {:id 112 :start "2012-01-01" :end "2012-12-31" :users_id 3 :clubs_id 2}
    
    {:id 204 :start "2012-01-01" :end "2012-12-31" :users_id 26 :clubs_id 4}))

(defn populate-participations
  [db] (sql/insert! db  :participations
         [:memberships_id :meetings_id]
         [100 8]
         [3 4]))

(defn setup-database
  [db] 
   (sql/db-do-commands db 
                       create-users
                       create-authentications
                       create-clubs
                       create-meetings
                       create-memberships
                       create-participations))

(defn populate-database
  [db]
   (populate-users db)
   (populate-clubs db)
   (populate-meetings db)
   (populate-memberships db)
   (populate-participations db))

(defn database-fixture [f] (default-connection sqlite-db-test)
                           (drop-database sqlite-db-test)
                           (setup-database sqlite-db-test)
                           (populate-database sqlite-db-test)
                           (f)
                           (drop-database sqlite-db-test)
                           (default-connection sqlite-db))
(defn schema-create
   []
   (if (= false (.exists (io/as-file "resources/db/rotary.db")))
     (do 
       (setup-database sqlite-db))))