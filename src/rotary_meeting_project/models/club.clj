(ns rotary-meeting-project.models.club (:require
  [rotary-meeting-project.models.util :as u]
  [rotary-meeting-project.models.sqlite :as sqlite]
  [korma.core :refer :all]
  [clj-time.core :as t]
  [bouncer.core :as b]
  [bouncer.validators :as v]
  [rotary-meeting-project.models.kormaentities :refer :all]))

(defn get-clubs
  "Return clubs"
  []
  (select clubs))

(defn get-clubs-res
  "Return clubs wrapped in ring-response"
  []
  (u/ok (get-clubs)))

(defn get-club
  [id]
  (first (select clubs (where {:id id}))))

(defn get-club-res
  "get club by id wrapped in response"
  [id]
  (if-let [result (get-club id)]
    (u/ok result)
    (u/not-found {:error "Club not found"})))

(defn create-club
  [club]
  (insert clubs (values club)))

(defn update-club
  [club]
  (update clubs (set-fields club) (where {:id (:id club)})))

(defn save-club-res
  "save club - response version"
  [club]
  (if (nil? (:id club))
    (u/save-ok (get-club (sqlite/last-id (create-club club))))
    (if (= 0 (update-club club))
      (u/update-failed "Cannot update the club")
      (u/save-ok club))))

(defn get-club-members
  ([club-id] (get-club-members club-id (t/now)))
  ([club-id date] (select memberships
                  (fields :memberships.id :memberships.start :memberships.end)
                  (with users (fields :users.image_url [:users.id :users_id] :users.firstname :users.surname)) 
                  (where {:clubs_id club-id})
                  (where {:memberships.start [<= date]})
                  (where 
                    (or {:memberships.end [>= date]}
                        {:memberships.end nil})))))

(defn get-club-members-res
  [id date]
  (let [validation-rules {:id [v/required v/number]}
        validation-map {:id id}]
    (if (b/valid? validation-map validation-rules)
      (if (not (nil? (get-club id))) 
        (if (nil? date)
          (get-club-members id (t/now))
          (get-club-members id date))
        (u/bad-request {:error "Club cannot be found"}))
    (u/bad-request {:error (b/validate validation-map validation-rules)}))))
  