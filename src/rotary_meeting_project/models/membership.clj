(ns rotary-meeting-project.models.membership
  (:require [rotary-meeting-project.models.sqlite :as sqlite]
            [rotary-meeting-project.models.util :as u]
            [rotary-meeting-project.models.kormaentities :refer :all]
            [clojure.java.io :as io]
            [bouncer.core :as b]
            [bouncer.validators :as v]
            [korma.core :refer :all]))

(defn get-membership
  [id]
  (first (select memberships (where {:id id}))))

(defn get-membership-res
  [id]
  (if-let [result (get-membership id)]
    (u/ok result)
    (u/not-found {:error "membership not found"})))

;; TODO needs failed update logic and return updated membership
(defn update-membership-res
  [membership] 
    (update memberships 
            (set-fields membership) 
            (where {:id (:id membership)}))
 (u/ok "ok"))

;; TODO input validation
(defn save-membership-res
  [membership]
  (insert memberships (values membership))
  (u/save-ok "ok"))
