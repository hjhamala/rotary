(ns rotary-meeting-project.models.kormaentities 
  (:require
    [rotary-meeting-project.models.sqlite :as sqlite]
    [korma.core :refer :all]
    [korma.db :refer :all]))

(defdb korma-db sqlite/sqlite-db)

(declare clubs meeting participations memberships meetings users authentications)

(defentity clubs)

(defentity meetings
  (belongs-to clubs))

(defentity participations
  (belongs-to meetings))

(defentity users
  (pk :id) 
  (has-many memberships)
  (many-to-many participations :memberships)
  (many-to-many clubs :memberships))

(defentity memberships
  (belongs-to clubs)
  (belongs-to users)
  (has-many participations))

(defentity authentications)
