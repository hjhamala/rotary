(ns rotary-meeting-project.models.authentication
(:require   [korma.core :refer :all]
            [korma.db :refer :all]
            [rotary-meeting-project.models.kormaentities :refer :all]
            [rotary-meeting-project.models.util :as u]
            [rotary-meeting-project.models.user :as user]
            [clj-time.core :as t]
            [clj-time.core :as t]
            [ring.util.response :as res]
            [buddy.core.nonce :as nonce]
            [buddy.core.codecs :as codecs]
            [buddy.auth :refer [authenticated? throw-unauthorized]]
            [buddy.auth.backends.token :refer [token-backend]]
            [buddy.hashers :as hs]
            [buddy.auth.accessrules :refer (success error)]))

(defn authenticate
  [req token]
  (if-let [result (first (select authentications (where {:valid [>= (t/now)] :token token})))]
    (do
      (update authentications (set-fields {:valid (t/from-now (t/minutes 30))}) (where {:token token}))
      (:users_id result))
    nil))

(defn random-token
  []
  (let [randomdata (nonce/random-bytes 16)]
    (codecs/bytes->hex randomdata)))

(defn login-res[users_name password]
  (if-not (or (nil? users_name) (nil? password))
    (if-let [result (first (select users (where {:username users_name})))]
      (if (hs/check password (:password result))
        (let [token (random-token)]
          (insert authentications (values [{:users_id (:id result)
                                           :valid (t/from-now (t/minutes 30))
                                           :token token}])) 
          (res/response {:token token :user (user/getuser-public-info (:id result))}))
        ;; Wrong password
        (u/bad-request "Wrong username or password"))
      ;; User not found
      (u/bad-request "Wrong username or password"))
    ;; Empty request
    (u/bad-request "Wrong username or password")))

(defn logoff-res
  [token]
  (res/response (delete authentications (where {:token token}))))

(defn authenticated-user
  [request]
  (if (authenticated? request)
    true
    (error (u/unauthorized {:error "Please login"}))))

(defn any-access
  [request]
  true)

(defn on-error
  [request value]
  {:status 403
   :headers {}
   :body "Not authorized"})

(def access-rules [{:pattern #"^/login$"
                    :handler any-access}
                   {:uri "/"
                    :handler any-access}
                   {:pattern #"^/js/.*"
                    :handler any-access}
                   {:pattern #"^/css/.*"
                    :handler any-access}
                   {:pattern #"^/partials/.*"
                    :handler any-access}
                   {:pattern #"^/appimg/.*"
                    :handler any-access}
                   {:pattern #"^/.*"
                    :handler authenticated-user}])