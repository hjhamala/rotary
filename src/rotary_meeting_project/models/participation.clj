(ns rotary-meeting-project.models.participation 
  (:require 
    [ring.util.response :as res]
    [korma.core :refer :all]
    [rotary-meeting-project.models.kormaentities :refer :all]
    [rotary-meeting-project.models.util :as u]
    [rotary-meeting-project.models.meeting :as meeting]
    [rotary-meeting-project.models.user :as user]))

(defn participate 
  [meetings_id memberships_id]
  (if (empty? (select participations 
                      (where {:meetings_id meetings_id
                              :memberships_id memberships_id})))
    (insert participations (values {:meetings_id meetings_id
                                    :memberships_id memberships_id}))))

(defn unparticipate 
  [meetings_id memberships_id]
  (delete participations (where {:meetings_id meetings_id
                                 :memberships_id memberships_id})))

(defn participate-res 
  [meetings_id memberships_id]
  (let [mtg (meeting/get-meeting meetings_id) 
        mbr (first (select memberships 
                           (where {:memberships.id memberships_id
                                   :memberships.start [<= (:meeting_date mtg)] 
                                   :memberships.clubs_id (:clubs_id mtg)})
                           (where 
                             (or {:memberships.end [>= (:meeting_date mtg)]} 
                                 {:memberships.end nil}))))]
    (if (or (nil? mtg) (nil? mbr))
      (u/bad-request {:error "Cannot participate"})
      (u/ok (participate meetings_id memberships_id)))))
      
(defn unparticipate-res 
  [meetings_id memberships_id]
  (u/ok (unparticipate meetings_id memberships_id)))