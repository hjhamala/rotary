(ns rotary-meeting-project.club_test
   (:require [clojure.test :refer :all]
            [ring.mock.request :as mock]
            [rotary-meeting-project.handler :refer :all]
            [rotary-meeting-project.models.club :as c]
            [rotary-meeting-project.models.sqlite :as sqlite]
            [clj-time.core :as t]
            [korma.core :refer :all]
            [korma.db :refer :all]
            [rotary-meeting-project.models.kormaentities :refer :all]))

(use-fixtures :each sqlite/database-fixture)

(deftest test-get-members 
  (testing 
    "Invalid and empty members list"
    (is (empty? (c/get-club-members 0)))
    (is (empty? (c/get-club-members 4 (t/date-time 2011 1 2)))))
  (testing 
    "real club lists"
    (is (= 3 (count (c/get-club-members 4))))
    (is (= 1 (count (c/get-club-members 4 (t/date-time 2012 6 30)))))))
           
  

