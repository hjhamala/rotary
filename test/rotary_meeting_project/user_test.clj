(ns rotary-meeting-project.user_test
  (:require [clojure.test :refer :all]
            [ring.mock.request :as mock]
            [rotary-meeting-project.handler :refer :all]
            [rotary-meeting-project.models.user :refer :all]
            [rotary-meeting-project.models.sqlite :as sqlite]
            [korma.core :refer :all]
            [korma.db :refer :all]
            [rotary-meeting-project.models.kormaentities :refer :all]))

(use-fixtures :each sqlite/database-fixture)

(deftest test-get-user 
  (is (= 400 (:status (getuser-res "K"))))
  (is (= 404 (:status (getuser-res 0))))
  (is (= 1 (:id (:body (getuser-res 1))))))

(deftest get-users
  (is (= (count (:body (getusers-res))) 4)))

(deftest add-user
  (is (= 400 (:status (saveuser-res {})))             
  (let [added-user (saveuser-res {:firstname "Reijo" :surname "Kärkkäinen"})]
        (is (= 201 (:status added-user)))  
        (is (= "Reijo" (-> added-user :body :firstname )))
        (is (= "Kärkkäinen" (-> added-user :body :surname )))
        (is (number? (-> added-user :body :id ))))))

(deftest test-save-user(let [user (:body (getuser-res 1))]
                         (saveuser-res (assoc-in user [:firstname] "Heikki J"))
                         (is (= "Heikki J" (-> (getuser-res 1) :body :firstname)))
                         (is (= 400 (:status (saveuser-res (dissoc user :firstname)))))
                         (is (= 422 (:status (saveuser-res {:id 0 :firstname "Heikki" :surname "Malinen"}))))))

(deftest test-delete-user
  (deleteuser-res 0)
  (is (= 400 (:status (deleteuser-res "S")))) 
  (is (= (count (:body (getusers-res))) 4))  
  (is (= 200 (:status (deleteuser-res 1))))
  (is (= 200 (:status (deleteuser-res 3))))
  (is (= 200 (:status (deleteuser-res 8))))
  (is (= 1 (count (getusers)))))
                     
(deftest test-get-club-history
  (is (= 400 (:status (get-club-history-res "S")))) 
  (let [clubs (get-club-history 3)]
    (is (= (count clubs) 2))
  (let [club1 (first clubs) club2 (first (rest clubs))]
    (is (= (:name club1) "Kampin Rotaryklubi"))
    (is (= (:name club2) "Kluuvin Rotaryklubi")))))
