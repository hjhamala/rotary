(ns rotary-meeting-project.participation_test 
  (:require [clojure.test :refer :all]
            [ring.mock.request :as mock]
            [rotary-meeting-project.handler :refer :all]
            [rotary-meeting-project.models.participation :refer :all]
            [rotary-meeting-project.models.meeting :as meeting]
            [rotary-meeting-project.models.sqlite :as sqlite]
            [clojure.java.jdbc :as sql]
            [korma.core :refer :all]
            [korma.db :refer :all]
            [rotary-meeting-project.models.kormaentities :refer :all]))

(use-fixtures :each sqlite/database-fixture)

(deftest test-participate 
  (let [meeting_id (sqlite/last-id (first (sql/insert! sqlite/sqlite-db-test :meetings
                                                 {:meeting_date "2015-04-10" 
                                                  :topic "Perustulo" :clubs_id 4})))]
    (participate meeting_id 100)
        (let [participation_list (:body (meeting/get-participation-list-res meeting_id))]                                  
          (let [user (filter (fn[x](if (= 1 (:id x)) x)) participation_list)]
            (is (= 1 (:id (first user))))))))