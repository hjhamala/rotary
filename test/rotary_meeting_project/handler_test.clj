(ns rotary-meeting-project.handler-test
  (:require [clojure.test :refer :all]
            [ring.mock.request :as mock]
            [rotary-meeting-project.handler :refer :all]
            [rotary-meeting-project.models.sqlite :as sqlite]
            [korma.core :refer :all]
            [korma.db :refer :all]
            [clojure.data.json :as json]))

(use-fixtures :each sqlite/database-fixture)

(deftest test-app
  (testing "not-found route"
    (let [response (app (mock/request :get "/invalid"))]
      (is (= (:status response) 401)))))

(deftest test-login
  (testing "login process"
    (let [token (:token (json/read-str (:body (app (mock/request :post "/login" {:username "hjhamala" :password "heikki"}))) :key-fn keyword))]
      (let [response (app (assoc-in (mock/request :get "/user") [:headers] {"host" "localhost" "Authorization" (str "Token " token)}))]
        (is (= 200 (:status response)))))))
                       
           

(deftest test-save-user
  (testing "save-user route (and fail)"
   (let [response (app (mock/request :post "/user" {:first_name "Pentti" :id "K"}))]
     (is (= (:status response) 401)))))




           
           

