(ns rotary-meeting-project.meeting_test
  (:require [clojure.test :refer :all]
            [ring.mock.request :as mock]
            [rotary-meeting-project.handler :refer :all]
            [rotary-meeting-project.models.user :as user]
            [rotary-meeting-project.models.meeting :as m]
            [rotary-meeting-project.models.sqlite :as sqlite]
            [korma.core :refer :all]
            [korma.db :refer :all]
            [rotary-meeting-project.models.kormaentities :refer :all]))

(use-fixtures :each sqlite/database-fixture)

(deftest test-get-meeting-statistics-res
  (let [statistics (:body (m/get-meeting-statistics-res 8))]
    (is (= 1 (:participating_count statistics)))
    (is (= 2 (:not_participating_count statistics)))
    (is (= 3 (:total statistics)))))

(deftest test-participating-count
  (let [count (m/participating-count 8)]
    (is (= 1 count))))

(deftest test-not-participating-count
  (let [count (m/not-participating-count 8)]
    (is (= 2 count))))

(deftest test-save-meeting-res
  (is (= 400 (:status (m/save-meeting-res {}))))
  (is (= 201 (:status (m/save-meeting-res  {:meeting_date "2013-03-03"
                                            :presenter "Teuvo Väiski"
                                            :topic  "Maidon kaksihintajärjestelmä"
                                            :clubs_id 4} ))))
  (let [mtg (m/get-meeting 8)]
    (is (= 200 (:status (m/save-meeting-res (assoc-in mtg [:presenter] "Urpo tuoppi"))))))) 