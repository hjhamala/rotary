(ns rotary-meeting-project.authentication_test
   (:require [clojure.test :refer :all]
            [ring.mock.request :as mock]
            [rotary-meeting-project.models.authentication :refer :all]
            [rotary-meeting-project.models.sqlite :as sqlite]
            [korma.core :refer :all]
            [korma.db :refer :all]
            [rotary-meeting-project.models.kormaentities :refer :all]))

(use-fixtures :each sqlite/database-fixture)

(deftest test-login
  (is (= 400 (:status (login-res "hjhamala" "hei"))))
  (is (= 200 (:status (login-res "hjhamala" "heikki")))))
