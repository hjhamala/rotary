var rotaryControllers = angular.module('rotaryControllers', ['angularFileUpload']);

rotaryControllers.controller('UserListCtrl', function ($scope, User) {
	$scope.users = User.query();
	$scope.orderProp = 'surname';
});

rotaryControllers.controller('UserDetail', 
	    function ($scope, $routeParams, User, ClubHistory) {
		$scope.edit = false;
		$scope.user = User.get({id: $routeParams.userId});
		$scope.club_history = ClubHistory.query({id: $routeParams.userId});
});

rotaryControllers.controller('UserCtrl', 
    function ($scope, $routeParams, $http, $location, $upload, $modal, User, Club, ClubHistory, Membership) {
	
	$scope.edit = true;
	if ($routeParams.userId){
		$scope.user = User.get({id: $routeParams.userId});
		$scope.club_history = ClubHistory.query({id: $routeParams.userId});
		$scope.clubs = Club.query();
	} 
	
	$scope.selectMembership = function (id){
		$scope.membership = Membership.get({id: id}, function(data){
			$scope.membership.start = new Date(data.start);
			if (data.end != null) {$scope.membership.end = new Date(data.end)};
		});
	};
	
	$scope.saveMembership = function (){
		var membership = new Membership($scope.membership);
		membership.users_id = $scope.user.id;
		membership.$save();
	}
	
	$scope.upload = function (files) {
	        if (files && files.length) {
	            for (var i = 0; i < files.length; i++) {
	                var file = files[i];
	                $upload.upload({
	                    url: '/user/' + $scope.user.id + '/image',
	                    fields: {},
	                    file: file
	                }).progress(function (evt) {
	                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
	                    console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
	                }).success(function (data, status, headers, config) {
	                	$scope.user.image_url = data.image_url;
	                });
	            }
	        }
	    };
	
	$scope.saveUser = function () {
		var user = new User($scope.user);
		user.$save([], function(user){
			$location.path('/user/' + user.id);
		})
	};

	$scope.openDeleteModal = function () {
		    $modal.open({
		      templateUrl: 'partials/user-delete-modal.html',
		      controller: 'ModalInstanceCtrl',
		      resolve: {
		    	  id: function(){
		    		  return $routeParams.userId;
		    	  }
		      }
		    });
	 };
});

rotaryControllers.directive('closeModal', function() {
	   return {
	     restrict: 'A',
	     link: function(scope, element, attr) {
	       scope.dismiss = function() {
	           element.modal('hide');
	       };
	     }
	   } 
	});


rotaryControllers.controller('ModalInstanceCtrl', function ($scope, $modalInstance, $location, User,id) {

	  $scope.ok = function () {
		  User.delete({id: id});
		  $modalInstance.close();
		  $location.path('/');
		  
	  };

	  $scope.cancel = function () {
	    $modalInstance.dismiss();
	  };
	});

rotaryControllers.controller('Login', 
	    function ($scope, $routeParams, Login, $location) {
	
	if (localStorage.getItem("token")){
		try {
			$scope.user = JSON.parse(localStorage.getItem("user"));	
		} catch (error) {
			// Propably session got corrupted
			localStorage.removeItem('user');
		}
				
	}
	
	$scope.login = function(){
		var login = new Login({username: $scope.username, password: $scope.password});
		login.$login([], function (data){
							// Success
							localStorage.setItem("token", data.token);
							localStorage.setItem('user', JSON.stringify(data.user));
							$scope.username = null;
							$scope.password = null;
							$scope.user = data.user;
							$location.path('/user');
						},
						function (data){
							// Failure
							$('#loginPopper').popover('show');
							setTimeout(function(){
								$('#loginPopper').popover('hide');
							}, 2000)
						});
	};
	
	$scope.logoff = function(){
		Login.logoff({token: localStorage.getItem('token')}, function (data){
							// Success
							localStorage.removeItem("token");
							localStorage.removeItem('user');
							$scope.user = null;
							$location.path("/");
						},
						function (data){
							// Failure
							console.log("Cant logoff")
						});
	};
	
	$scope.testLogin = function(){
		Login.get();
	};	
});

