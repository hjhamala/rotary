var rotaryServices = angular.module('rotaryServices', ['ngResource']);

rotaryServices.factory('User', ['$resource',
  function($resource){
    return $resource('user/:id', {}, {
      query: {method:'GET', params:{}, isArray:true}
    });
  }]);

rotaryServices.factory('ClubHistory', ['$resource',
                                function($resource){
                                  return $resource('user/:id/club-history', {}, {
                                    query: {method:'GET', params:{id: 'id'}, isArray:true}
                                  });
                                }]);


rotaryServices.factory('Meeting', ['$resource',
                                function($resource){
                                  return $resource('meeting/:id', {}, {
                                    query: {method:'GET', params:{}, isArray:true}
                                  });
                                }]);

rotaryServices.factory('ParticipationList', ['$resource',
                                   function($resource){
                                     return $resource('meeting/:id/participation-list', {}, {
                                       query: {method:'GET', params:{id: 'id'}, isArray:true}
                                     
                                     });
                                   }]);

rotaryServices.factory('Participation', ['$resource',
                                             function($resource){
                                               return $resource('participation/:meetings_id/:memberships_id', {}, {
                                               });
                                             }]);

rotaryServices.factory('ParticipationStatistics', ['$resource',
                                         function($resource){
                                           return $resource('meeting/:meeting_id/participation-statistics', {}, {
                                           });
                                         }]);

rotaryServices.factory('Club', ['$resource',
                                                   function($resource){
                                                     return $resource('club/:clubs_id', {}, {
                                                    	 getMemberships: {method: 'GET', url: 
                                                    		 'club/:clubs_id/memberships', params:{clubs_id: '@clubs_id'}, isArray:true}
                                                     });
                                                   }]);

rotaryServices.factory('Membership', ['$resource',
                                function($resource){
                                  return $resource('membership/:id', {id: '@id'}, {
                                  });
                                }]);

rotaryServices.factory('Login', ['$resource',
                                      function($resource){
                                        return $resource('login/:token', {}, {
                                        	login: {method:'POST', params:{}, isArray:false},
                                        	logoff: {method: 'DELETE', params:{token: '@token'}, isArray:false}
                                        });
                                      }]);


