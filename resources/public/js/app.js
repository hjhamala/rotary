var rotaryApp = angular.module('rotaryApp', [
  'ngRoute',
  'ui.bootstrap',
  'rotaryControllers',
  'rotaryServices'
]);

/**
 * Interceptor for injectin session token 
 */
rotaryApp.factory('sessionInjector', [function() {  
    var sessionInjector = {
        request: function(req) {
            if (localStorage.getItem("token")){
            	req.headers['Authorization'] = "Token " + localStorage.getItem("token");
            }
        	
            return req;
        }
    };
    return sessionInjector;
}]);

/**
 * Interceptor for handling authentication/authorization errors
 */

rotaryApp.factory('errorInjector', function($rootScope, $q) {  
    var errorInjector = {
        responseError: function(res) {
             if (res.status == 401){
              $rootScope.$broadcast('loginRequired');            
             }
             
             if (res.status == 403){
              $rootScope.$broadcast('forbidden');            
             }
             
            return $q.reject(res);
        }
    };
    return errorInjector;
});


rotaryApp.config(['$httpProvider', function($httpProvider) {  
    $httpProvider.interceptors.push('sessionInjector');
    $httpProvider.interceptors.push('errorInjector');
}]);

/**
 * This is needed for redirecting user to unauthorized page
 */
rotaryApp.run(function($rootScope, $location){
      $rootScope.$on('loginRequired', function() {
        $location.path('/unauthorized');
      });
      
      $rootScope.$on('forbidden', function() {
        $location.path('/forbidden');
      });
});

rotaryApp.config(['$routeProvider',
                    function($routeProvider) {
                      $routeProvider.
                        when('/user/', {
                          templateUrl: 'partials/user-list.html',
                          controller: 'UserListCtrl'
                        }).
                        when('/user/edit/:userId', {
                            templateUrl: 'partials/user-edit.html',
                            controller: 'UserCtrl'
                          }).
                        when('/user/edit/', {
                              templateUrl: 'partials/user-edit.html',
                              controller: 'UserCtrl'
                          }).
                        when('/user/:userId', {
                          templateUrl: 'partials/user-edit.html',
                          controller: 'UserDetail'
                        }).
                        when('/meeting/edit', {
                            templateUrl: 'partials/meeting-edit.html',
                            controller: 'MeetingEditCtrl'
                          }).    
                        when('/meeting/:meetingId', {
                            templateUrl: 'partials/meeting-edit.html',
                            controller: 'MeetingDetailCtrl'
                          }).
                        when('/meeting/:meetingId/edit', {
                              templateUrl: 'partials/meeting-edit.html',
                              controller: 'MeetingEditCtrl'
                            }).
                            
                        when('/meeting/:meetingId/participation', {
                          templateUrl: 'partials/meeting-participation.html',
                          controller: 'MeetingDetailCtrl'
                        }).
                        when('/meeting/', {
                          templateUrl: 'partials/meeting-list.html',
                          controller: 'MeetingListCtrl'
                        }).
                        when('/club/', {
                          templateUrl: 'partials/club-list.html',
                          controller: 'ClubListCtrl'
                        }).
                        when('/club/:clubId/edit', {
                          templateUrl: 'partials/club-edit.html',
                          controller: 'ClubEditCtrl'
                        }).
                        when('/club/edit', {
                            templateUrl: 'partials/club-edit.html',
                            controller: 'ClubEditCtrl'
                          }).
                        when('/club/:clubId', {
                          templateUrl: 'partials/club-edit.html',
                          controller: 'ClubDetailCtrl'
                        }).
                        when('/unauthorized/', {
                          templateUrl: 'partials/unauthorized.html'
                        }).
                        when('/forbidden/', {
                          templateUrl: 'partials/forbidden.html'
                        }).
                        when('/', {
                        	templateUrl: 'partials/welcome.html' 	
                        }).
                        otherwise({
                          redirectTo: '/user/'
                        });
                    }]);