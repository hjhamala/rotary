rotaryControllers.controller('ClubListCtrl', 
	    function ($scope, $routeParams, Club) {
			Club.query([], function(result){
				$scope.clubs = result;
				for (var i = 0; i < $scope.clubs.length; i++) {
					if ($scope.clubs[i].established != null) {
						$scope.clubs[i].established = new Date($scope.clubs[i].established);
					};
				}
			});
		});

rotaryControllers.controller('ClubDetailCtrl', 
	    function ($scope, $routeParams, Club) {
	$scope.edit = false;
	
	Club.get({clubs_id: $routeParams.clubId}, function (club){
		$scope.club = club;
		$scope.club.established = new Date(club.established);
	});
	
	Club.getMemberships({clubs_id: $routeParams.clubId}, function (data){
		$scope.memberships = data;
		for (var i = 0; i < $scope.memberships.length; i++) {
			if ($scope.memberships[i].start != null) {
				$scope.memberships[i].start= new Date($scope.memberships[i].start);
			};
		}
	})
});

rotaryControllers.controller('ClubEditCtrl', 
    function ($scope, $routeParams, $http, $location, $modal, Club) {
	
	$scope.edit = true;
	if ($routeParams.clubId){
		Club.get({clubs_id: $routeParams.clubId}, function (club){
			$scope.club = club;
			$scope.club.established = new Date(club.established);
		});
		
	}
	
	$scope.saveClub= function () {
		var club = new Club($scope.club);
		club.$save([], function(club){
			$location.path('/club/' + club.id);
		})
	};
	
});
