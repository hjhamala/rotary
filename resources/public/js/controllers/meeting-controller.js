rotaryControllers.controller('MeetingListCtrl', function ($scope, $http, Meeting) {
	$scope.meetings = Meeting.query();
	$scope.orderProp = 'club';
});

rotaryControllers.controller('MeetingDetailCtrl', 
                                                function ($scope, $routeParams,$http,$location, $route, Meeting, ParticipationList, Participation, ParticipationStatistics, Club) {
	$scope.edit = false;
	$scope.meeting = Meeting.get({id: $routeParams.meetingId}, function(data){
		$scope.meeting.meeting_date = new Date(data.meeting_date);
	});
	$scope.clubs = Club.query();
	$scope.participantslist = ParticipationList.query({id: $routeParams.meetingId}); 
	$scope.stats = ParticipationStatistics.get({meeting_id: $routeParams.meetingId});
	
	$scope.participated_percentage = function(){
		return Math.round($scope.stats.participating_count/$scope.stats.total*100);
	};
	
	$scope.not_participated_percentage = function(){
		return Math.round($scope.stats.not_participating_count/$scope.stats.total*100);
	};
	
	$scope.editParticipations = function(){
		$location.path('/meeting/' + $scope.meeting.id + '/participation');
	};
	
	$scope.participate = function (memberships_id) {
		var participation = new Participation({meetings_id: $scope.meeting.id, memberships_id: memberships_id})
		participation.$save([], function(){
			$scope.participantslist.filter(function(a){return a['memberships.id'] == memberships_id})[0].participation = true;
		});
	
	};

	$scope.unparticipate = function (memberships_id) {
		Participation.delete({meetings_id: $scope.meeting.id, memberships_id: memberships_id}, function(){
			$scope.participantslist.filter(function(a){return a['memberships.id'] == memberships_id})[0].participation = false;
		});
	};
});

rotaryControllers.controller('MeetingEditCtrl', 
	    function ($scope, $routeParams, $location, Meeting, Club) {
		
		$scope.edit = true;
		$scope.clubs = Club.query();

		if ($routeParams.meetingId){
			$scope.meeting = Meeting.get({id: $routeParams.meetingId}, function(data){
				$scope.meeting.meeting_date = new Date(data.meeting_date);
			});
		} 
		
		$scope.saveMeeting = function () {
			Meeting.save($scope.meeting, function(meeting, status){
				
				$location.path('/meeting/' + meeting.id);
				
			}, function(status){
				$scope.server_error = true;
			})
		};
	}
);


