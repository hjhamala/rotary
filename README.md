# rotary-meeting-project

The Rotary meeting projects aim is to give tools to help administrate club meetings and participations of members.

## Aims
* Create and edit members and their memberships in clubs
* Track participations of the members in meetings
* give statistics of meetings
* create RESTlike API for integration with 3rd party systems.

### Benefits for secretary
* simplify tracking participations, not more paperwork, member info have also pictures

### Benefits for member:
* able to check participation info 
* optionally allow the members to update themselves participations

## Long term plans:
Allow member authentication via 3rd party systems like Facebook, Twitter, Google etc.

## Current status

### What works:
* adding, editing and removing users
* adding and editing memberships (not removing yet)
* adding and editing clubs (not removing)
* adding and editin meetings (not removing)
* participating in meetings
* Simple authorization and authentication

## TODO:
* input validators for every single operation
* routes are becoming messy in angular. Maybe they can be split?
* stateless session. Maybe system with refresh-tokens and intereptor handling the token refreshment? 
* what to do to participations if users membership ends. Perhaps delete participations which are later than the memberships end date.
* add authorization via buddy
* editing earlier memberships is confusing in the gui. Maybe form should be horizontally positioned to table than vertically?

## Architechture

## Server:
Clojure running
 
* Ring 
* Compojure (routing)
* Korma (sql)
* Buddy (authentication and authorization)
* Sqlite (database)

## Client:
Angularjs single page application (SPA) with Bootstrap. All the interaction between the server and the client happens via RESTlike/json interface after the initial downloading of SPA.

## Prerequisites

You will need [Leiningen][] 2.0.0 or above installed.

[leiningen]: https://github.com/technomancy/leiningen

You must download necessary Javascript libraries. This can be done via Bower, run:

    bower install

## Running

To start a web server for the application, run:

    lein run

You can prepopulate database by using function populate-database which can be found in model.sqlite.

## Projects background

This project started mainly to become a portfolio project for job applications. 

Of course it will be very nice if some day rotary clubs around the world can use this to make easier to administrate meetings :)

## License

GPL v. 2 https://www.gnu.org/licenses/gpl-2.0.html