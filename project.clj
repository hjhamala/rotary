(defproject rotary-meeting-project "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :min-lein-version "2.0.0"
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [compojure "1.3.1"]
                 [ring/ring-core "1.3.2"]
                 [ring/ring-json "0.3.1"]
                 [ring/ring-defaults "0.1.2"]
                 [ring/ring-jetty-adapter "1.2.1"]
                 [org.clojure/data.json "0.2.6"]
                 [korma "0.4.0"]
                 [org.clojure/java.jdbc "0.3.6"]
                 [clj-time "0.9.0"]
                 [bouncer "0.3.2"]
                 [com.novemberain/validateur "2.4.2"]
                 [org.xerial/sqlite-jdbc "3.8.7"]
                 [buddy "0.5.2"]
                 [org.clojure/data.json "0.2.6"]]
  
  :plugins [
            [lein-ring "0.8.13"]
            ]
  
  :ring {:handler rotary-meeting-project.handler/app}
  
  :main ^:skip-aot rotary-meeting-project.handler
  
  :profiles
  {:dev {:resource-paths ["resources/public/img"]
         :dependencies [[javax.servlet/servlet-api "2.5"]
                        [ring-mock "0.1.5"]]}
   :uberjar {:aot :all}})
